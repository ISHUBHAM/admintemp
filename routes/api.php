<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([

    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'JwtController@login');
    Route::post('logout', 'JwtController@logout');
    Route::post('refresh', 'JwtController@refresh');
    Route::post('me', 'JwtController@me');

});


Route::get('/alluser','ApiController@allUser');
Route::get('/allpost','ApiController@allPost');
Route::get('/specificdata/{id}','ApiController@specificData');
Route::get('/update/{id}','ApiController@update');
Route::get('/delete/{id}','ApiController@delete');
Route::get('/betweendate','ApiController@betweendate');
Route::get('/userhavepost','ApiController@userhavepost');


Route::get('/loginapi/{email}/{password}','ApiController@loginApi');
// login Api

Route::post('/registerapi','ApiController@registerApi');
// register Api


Route::get('/posthavedate/{id}','ApiController@posthavedate');
// specific post having a specific date


Route::get('/pdfsend/{check}','ApiController@pdfsend');








