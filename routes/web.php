<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/clogin', function () {
    return view('clogin');
});


Route::get('/cregister', function () {
    return view('cregister');
});


Route::get('/cforget', function () {
    return view('cforget');
});

Route::get('/check1', function () {
    return redirect(url('/clogin'));
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/save', 'AuthController@save')->name('save')->name('cregister');
Route::post('/loginsave','AuthController@loginsave')->name('clogin');

Route::group(['middleware' => ['cauth']], function () {
Route::group(['prefix' =>'homepage'],function()
{
    Route::get('/','HomeController@getHomepage')->middleware('cauth');
    Route::get('/index1','HomeController@getDashboard1');
    Route::get('/index2','HomeController@getDashboard2');
    Route::get('/index3','HomeController@getDashboard3');
    Route::get('/widgate','HomeController@widgate');

});

});


Route::group(['prefix'=>'/addnav'],function(){

    Route::get('/topnav','LayoutController@topNav');
    Route::get('/topnavsidebar','LayoutController@topNavSidebar');
    Route::get('/boxed','LayoutController@boxed');
    Route::get('/fixedsidebar','LayoutController@fixedSidebar');
     Route::get('/fixedtopnav','LayoutController@fixedNavbar');
     Route::get('/fixedfooter','LayoutController@fixedFooter');
     Route::get('/collapsedsidebar','LayoutController@collapsedSidebar');
     Route::get('/calender','LayoutController@calender');
     Route::get('/gallery','LayoutController@gallery');
   });


   Route::group(['prefix'=>'/addnav/charts'],function(){
    Route::get('/chart','ChartController@chart');
     Route::get('/float','ChartController@float');
    Route::get('/inline','ChartController@inline');
   });


   Route::group(['prefix'=>'/addnav/tables'],function(){

    Route::get('/usertable','TableController@data')->name('usertable'); 
    Route::get('/profiletable','TableController@profile')->name('profiletable'); 
    Route::get('/posttable','TableController@post')->name('posttable');    
   


   });






