<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Profile extends Model
{
     function myuser()
     {
        return $this->belongsTo(User::class);

     }
}
