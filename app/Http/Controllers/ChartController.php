<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function __construct()
 {
    $this->middleware('cauth', ['only' => ['chart','float','inline']]); 

 }
public function chart()
{
    return view('addnav.charts.chart');
}
public function float()
{
    return view('addnav.charts.float');
}

public function inline()
{
    return view('addnav.charts.inline');
}

}
