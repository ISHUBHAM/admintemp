<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Profile;
use DB;


class TableController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('cauth');  
    }
    
    public function  data()
    {
           $user = DB::table('users')
                ->select('*')
                ->get();
            return view('addnav.tables.data',compact('user'));

    }


    public function post()

    {
        $post = DB::table('posts')
        ->select('*')
        ->get();
    return view('addnav.tables.jsgrid',compact('post'));


    }


    public function profile()

    {
        $profile = DB::table('profiles')
        ->select('*')
        ->get();
    return view('addnav.tables.simple',compact('profile'));
    }
  


}

